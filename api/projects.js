"use strict";
const service = require("../services/projects");
const response = require("../exchange/response");



const create = async (req, res) => {
    const log = req.context.logger.start(`api:projects:create`);
    try {
        const project = await service.create(req.body, req.context);
        const message = "Project Create Successfully..!!!";
        log.end();
        return response.success(res, message, project);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const getProjects = async (req, res) => {
    const log = req.context.logger.start(`api:projects:getProjects`);
    try {
        const project = await service.getProjects(req.body, req.context);
        const message = "Projects get Successfully..!!!";
        log.end();
        return response.success(res, message, project);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const getById = async (req, res) => {
    const log = req.context.logger.start(`api:projects:getById`);
    try {
        const project = await service.getById(req.params.id, req.body, req.context);
        const message = "get project";
        log.end();
        return response.success(res, message, project);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const projectDelete = async (req, res) => {
    const log = req.context.logger.start(`api:projects:projectDelete`);
    try {
        const project = await service.projectDelete(req.params.id, req.context);
        log.end();
        return response.data(res, project);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

// const update = async (req, res) => {
//     const log = req.context.logger.start(`api:projects:update`);
//     try {
//         const project = await service.update(req.params.id, req.body, req.context);
//         log.end();
//         return response.data(res, project);
//     } catch (err) {
//         log.error(err);
//         log.end();
//         return response.failure(res, err.message);
//     }
// };

exports.create = create;
exports.getProjects = getProjects;
exports.getById = getById;
exports.projectDelete = projectDelete;
// exports.update = update;




