"use strict";
const service = require("../services/plans");
const response = require("../exchange/response");



const create = async (req, res) => {
    const log = req.context.logger.start(`api:plans:create`);
    try {
        const plan = await service.create(req.body, req.context);
        const message = "Plan Create Successfully..!!!";
        log.end();
        return response.success(res, message, plan);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const getPlans = async (req, res) => {
    const log = req.context.logger.start(`api:plans:getPlans`);
    try {
        const plan = await service.getPlans(req.body, req.context);
        const message = "Plans get Successfully..!!!";
        log.end();
        return response.success(res, message, plan);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const getById = async (req, res) => {
    const log = req.context.logger.start(`api:plans:getById`);
    try {
        const plan = await service.getById(req.params.id, req.body, req.context);
        const message = "get Plan";
        log.end();
        return response.success(res, message, plan);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const planDelete = async (req, res) => {
    const log = req.context.logger.start(`api:plans:planDelete`);
    try {
        const plan = await service.planDelete(req.params.id, req.context);
        log.end();
        return response.data(res, plan);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const update = async (req, res) => {
    const log = req.context.logger.start(`api:plans:update`);
    try {
        const plan = await service.update(req.params.id, req.body, req.context);
        log.end();
        return response.data(res, plan);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

exports.create = create;
exports.getPlans = getPlans;
exports.getById = getById;
exports.planDelete = planDelete;
exports.update = update;




