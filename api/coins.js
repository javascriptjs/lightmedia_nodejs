"use strict";
const service = require("../services/coins");
const response = require("../exchange/response");



const create = async (req, res) => {
    const log = req.context.logger.start(`api:coins:create`);
    try {
        const coin = await service.create(req.body, req.context);
        const message = "coin Create Successfully..!!!";
        log.end();
        return response.success(res, message, coin);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const getCoins = async (req, res) => {
    const log = req.context.logger.start(`api:coins:getCoins`);
    try {
        const coin = await service.getCoins(req.body, req.context);
        const message = "coins All get Successfully..!!!";
        log.end();
        return response.getAll(res, message, coin);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const getById = async (req, res) => {
    const log = req.context.logger.start(`api:coins:getById`);
    try {
        const coin = await service.getById(req.params.id, req.body, req.context);
        const message = "get coin";
        log.end();
        return response.success(res, message, coin);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const coinDelete = async (req, res) => {
    const log = req.context.logger.start(`api:coins:coinDelete`);
    try {
        const coin = await service.coinDelete(req.params.id, req.context);
        log.end();
        return response.data(res, coin);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const update = async (req, res) => {
    const log = req.context.logger.start(`api:coins:update`);
    try {
        const coin = await service.update(req.params.id, req.body, req.context);
        log.end();
        return response.data(res, coin);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};
const uploadLogo = async(req, res) => {
    const log = req.context.logger.start(`api:coins:uploadLogo`);
    try {
        const coin = await service.uploadLogo(req.params.id, req.files[0], req.context);
        const message = "Profile Picture Successfully";
        log.end();
        return response.success(res, message, coin);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

exports.create = create;
exports.getCoins = getCoins;
exports.getById = getById;
exports.coinDelete = coinDelete;
exports.update = update;
exports.uploadLogo = uploadLogo;





