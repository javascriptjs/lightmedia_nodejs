module.exports = [

    {
        name: "createCoin",
        properties: {
            coinName: {
                type: "string"
            },
            amount: {
                type: "string"
            },
            coinCode: {
                type: "string"
            }
        }
    }
];