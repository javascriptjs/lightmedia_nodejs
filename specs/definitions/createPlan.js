module.exports = [

    {
        name: "createPlan",
        properties: {
            planName: {
                type: "string"
            },
            amount: {
                type: "string"
            },
            coinCount: {
                type: "string"
            }
        }
    }
];