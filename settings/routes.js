"use strict";

const fs = require("fs");
const api = require("../api");
const specs = require("../specs");
const permit = require("../permit")
const validator = require("../validators");

var multer = require('multer');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        if (file.fieldname == 'csv') {
            cb(null, path.join(__dirname, '../', 'assets'));
        } else {
            cb(null, path.join(__dirname, '../', 'assets/images'));
        }
    },
    filename: function (req, file, cb) {
        if (file.fieldname == 'csv') {
            cb(null, file.originalname);
        } else {
            cb(null, Date.now() + file.originalname);
        }
    }
});

var upload = multer({ storage: storage, limits: { fileSize: 1024 * 1024 * 50 } });


const configure = (app, logger) => {
    const log = logger.start("settings:routes:configure");
    app.get("/specs", function (req, res) {
        fs.readFile("./public/specs.html", function (err, data) {
            if (err) {
                return res.json({
                    isSuccess: false,
                    error: err.toString()
                });
            }
            res.contentType("text/html");
            res.send(data);
        });
    });


    app.get("/api/specs", function (req, res) {
        res.contentType("application/json");
        res.send(specs.get());
    });

    //user api's //
    app.post(
        "/api/users/create",
        permit.context.builder,
        validator.users.create,
        api.users.create
    );

    app.post(
        "/api/users/login",
        permit.context.builder,
        validator.users.login,
        api.users.login
    );

    app.get(
        "/api/users/currentUser/:id",
        permit.context.validateToken,
        api.users.currentUser
    );

    app.put(
        "/api/users/changePassword/:id",
        permit.context.validateToken,
        api.users.changePassword,
        validator.users.changePassword,
    );

    app.put(
        "/api/users/update/:id",
        permit.context.validateToken,
        api.users.update
    );

    app.put(
        "/api/users/updateCoins/:id",
        permit.context.builder,
        api.users.updateCoins
    );

    app.delete(
        "/api/users/delete/:id",
        permit.context.validateToken,
        api.users.deleteUser
    );

    app.get(
        "/api/users/search",
        permit.context.validateToken,
        api.users.search
    );

    app.post(
        "/api/users/forgotPassword",
        permit.context.builder,
        api.users.forgotPassword
    );

    app.get(
        "/api/users/getUsers",
        permit.context.validateToken,
        api.users.getUsers
    );

    
    app.get(
        "/api/users/notifications",
        permit.context.validateToken,
        api.users.notifications
    );

    app.put(
        "/api/users/uploadProfilePic/:id",
        permit.context.builder,
        upload.single('image'),
        api.users.uploadProfilePic
    );

    app.post(
        "/api/plans/create",
        permit.context.builder,
        api.plans.create
    );

    app.get(
        "/api/plans/getPlans",
        permit.context.builder,
        api.plans.getPlans
    );

    app.get(
        "/api/plans/getById/:id",
        permit.context.validateToken,
        api.plans.getById
    );

    app.delete(
        "/api/plans/delete/:id",
        permit.context.validateToken,
        api.plans.planDelete
    );

    app.put(
        "/api/plans/update/:id",
        permit.context.validateToken,
        api.plans.update
    );

    app.post(
        "/api/projects/create",
        permit.context.builder,
        api.projects.create
    );

    app.get(
        "/api/projects/getProjects",
        permit.context.validateToken,
        api.projects.getProjects
    );

    app.get(
        "/api/projects/getById/:id",
        permit.context.validateToken,
        api.projects.getById
    );

    app.delete(
        "/api/projects/delete/:id",
        permit.context.validateToken,
        api.projects.projectDelete
    );

    app.post(
        "/api/coins/create",
        permit.context.builder,
        api.coins.create
    );

    app.get(
        "/api/coins/getCoins",
        permit.context.builder,
        api.coins.getCoins
    );

    app.get(
        "/api/coins/getById/:id",
        permit.context.validateToken,
        api.coins.getById
    );

    app.delete(
        "/api/coins/delete/:id",
        permit.context.validateToken,
        api.coins.coinDelete
    );

    app.put(
        "/api/coins/update/:id",
        permit.context.validateToken,
        api.coins.update
    );

    app.put(
        "/api/coins/uploadLogo/:id",
        permit.context.builder,
        upload.single('image'),
        api.coins.uploadLogo
    );


    log.end();
};

exports.configure = configure;