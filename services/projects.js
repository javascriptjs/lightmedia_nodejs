const create = async (model, context) => {
    const log = context.logger.start("services:projects:create");
    const isProject = await db.project.findOne({ projectName: { $eq: model.projectName } });
    if (isProject) {
        throw new Error("project is already exists");
    } else {
        const projects = await new db.project({
            projectName: model.projectName
        }).save();
        log.end();
        return projects;
    }
};

const getProjects = async (query, context) => {
    const log = context.logger.start(`services:projects:getProjects`);
    let allProjects = await db.project.find()
    allProjects.count = await db.project.find().count();
    log.end();
    return allProjects;
};

const getById = async (id, model, context) => {
    const log = context.logger.start(`services:projects:getById`);
    if (!id) {
        throw new Error("project id is required");
    }
    let project = await db.project.findById(id)
    if (!project) {
        throw new Error("project not found");
    }
    log.end();
    return project;
};

const projectDelete = async (id, context) => {
    const log = context.logger.start(`services:projects:projectDelete`);
    if (!id) {
        throw new Error("id is requried");
    }

    let project = await db.project.deleteOne({ _id: id });

    if (!project) {
        throw new Error("project not found");
    }
    log.end();
    return 'Project Deleted Successfully'
};

// const update = async (id, model, context) => {
//     const log = context.logger.start(`services:projects:update`);
//     let entity = await db.project.findById(id)
//     if (!entity) {
//         throw new Error("invalid project");
//     }
//     if (model.projectName !== "string" && model.projectName !== undefined) {
//         entity.projectName = model.projectName;
//     }
//     if (model.amount !== "string" && model.amount !== undefined) {
//         entity.amount = model.amount;
//     }
//     await entity.save();
//     log.end();
//     return entity
// };

exports.create = create;
exports.getProjects = getProjects;
exports.getById = getById;
exports.projectDelete = projectDelete;
// exports.update = update;


