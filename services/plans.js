const notificationService = require("./push-notification");

const create = async (model, context) => {
    const log = context.logger.start("services:plans:create");
    const isPlan = await db.plan.findOne({ planName: { $eq: model.planName } });
    if (isPlan) {
        throw new Error("Plan is already exists");
    } else {
        const plans = await new db.plan({
            planName: model.planName,
            amount: model.amount,
            coinCount: model.coinCount,
        }).save();
        notify(plans, context)
        log.end();
        return plans;
    }
};


const notify = async (plan, context) => {
    let body = `${plan.planName} Added A New Plan with ${plan.amount}`
    let title = `${plan.planName}`
    let users = await db.user.find()
    for (let user of users) {
        if (user.deviceToken) {
            await new db.notification({
                title: title,
                body: body,
                plan: plan._id,
                status: 'new'
            }).save();
            notificationService.pushNotification(user.deviceToken, title, body, context.logger)
        }
    }
}

const getPlans = async (query, context) => {
    const log = context.logger.start(`services:plans:getPlans`);
    let allPlans = await db.plan.find()
    allPlans.count = await db.plan.find().count();
    log.end();
    return allPlans;
};

const getById = async (id, model, context) => {
    const log = context.logger.start(`services:plans:getById`);
    if (!id) {
        throw new Error("plan id is required");
    }
    let plan = await db.plan.findById(id)
    if (!plan) {
        throw new Error("plan not found");
    }
    log.end();
    return plan;
};

const planDelete = async (id, context) => {
    const log = context.logger.start(`services:plans:planDelete`);
    if (!id) {
        throw new Error("id is requried");
    }

    let plan = await db.plan.deleteOne({ _id: id });

    if (!plan) {
        throw new Error("plan not found");
    }
    log.end();
    return 'Plan Deleted Successfully'
};

const update = async (id, model, context) => {
    const log = context.logger.start(`services:plans:update`);
    let entity = await db.plan.findById(id)
    if (!entity) {
        throw new Error("invalid plan");
    }
    if (model.planName !== "string" && model.planName !== undefined) {
        entity.planName = model.planName;
    }
    if (model.amount !== "string" && model.amount !== undefined) {
        entity.amount = model.amount;
    }

    if (model.coinCount !== "string" && model.coinCount !== undefined) {
        entity.coinCount = model.coinCount;
    }
    await entity.save();
    updateNotify(entity, context);
    log.end();
    return entity
};

const updateNotify = async (plan, context) => {
    let body = `${plan.planName} Plan is Updated`
    let title = `${plan.planName}`
    let users = await db.user.find().populate('plan');
    for (let user of users) {
        if (user.plan.id === plan.id) {
            if (user.deviceToken) {
                await new db.notification({
                    title: title,
                    body: body,
                    plan: plan._id,
                    status: 'edit'
                }).save();
                notificationService.pushNotification(user.deviceToken, title, body, context.logger)
            }
        }
    }
}

exports.create = create;
exports.getPlans = getPlans;
exports.getById = getById;
exports.planDelete = planDelete;
exports.update = update;


