const notificationService = require("./push-notification");


const create = async (model, context) => {
    const log = context.logger.start("services:coins:create");
    const iscoin = await db.coin.findOne({ coinName: { $eq: model.coinName } });
    if (iscoin) {
        throw new Error("coin is already exists");
    } else {
        const coins = await new db.coin({
            coinName: model.coinName,
            amount: model.amount,
            coinCode: model.coinCode
        }).save();
        notify(coins, context)
        log.end();
        return coins;
    }
};

const notify = async (coin, context) => {
    let body = `${coin.coinName} Added A New Coin with ${coin.amount}`
    let title = `${coin.coinName}`
    let users = await db.user.find()
    for (let user of users) {
        if (user.deviceToken) {
            await new db.notification({
                title: title,
                body: body,
                coin: coin._id,
                status: 'new'
            }).save();
            notificationService.pushNotification(user.deviceToken, title, body, context.logger)
        }
    }
}

const getCoins = async (query, context) => {
    const log = context.logger.start(`services:coins:getCoins`);
    let allcoins = await db.coin.find()
    allcoins.count = await db.coin.find().count();
    log.end();
    return allcoins;
};

const getById = async (id, model, context) => {
    const log = context.logger.start(`services:coins:getById`);
    if (!id) {
        throw new Error("coin id is required");
    }
    let coin = await db.coin.findById(id)
    if (!coin) {
        throw new Error("coin not found");
    }
    log.end();
    return coin;
};

const coinDelete = async (id, context) => {
    const log = context.logger.start(`services:coins:coinDelete`);
    if (!id) {
        throw new Error("id is requried");
    }

    let coin = await db.coin.deleteOne({ _id: id });

    if (!coin) {
        throw new Error("coin not found");
    }
    log.end();
    return 'Coin Deleted Successfully'
};

const update = async (id, model, context) => {
    const log = context.logger.start(`services:coins:update`);
    let entity = await db.coin.findById(id)
    if (!entity) {
        throw new Error("invalid coin");
    }
    if (model.coinName !== "string" && model.coinName !== undefined) {
        entity.coinName = model.coinName;
    }
    if (model.amount !== "string" && model.amount !== undefined) {
        entity.amount = model.amount;
    }
    if (model.coinCode !== "string" && model.coinCode !== undefined) {
        entity.coinCode = model.coinCode;
    }
    await entity.save();
    updateNotify(entity, context);
    log.end();
    return entity
};

const updateNotify = async (coin, context) => {
    let body = `${coin.coinName} Coin is Updated`
    let title = `${coin.coinName}`
    let users = await db.user.find().populate('coins');
    for (let user of users) {
        for (let item of user.coins) {
            if (item.id === coin.id) {
                if (user.deviceToken) {
                    await new db.notification({
                        title: title,
                        body: body,
                        coin: coin._id,
                        status: 'edit'
                    }).save();
                    notificationService.pushNotification(user.deviceToken, title, body, context.logger)
                }
            }
        }
    }
}

const uploadLogo = async (id, file, context) => {
    const log = context.logger.start(`services:coins:uploadLogo`);
    let coin = await db.coin.findById(id);
    if (!file) {
        throw new Error("image not found");
    }
    if (!coin) {
        throw new Error("coin not found");
    }
    if (coin.logo != "" && coin.logo !== undefined) {

        let picUrl = coin.logo.replace(`${imageUrl}`, '');
        try {
            await fs.unlinkSync(`${picUrl}`)
            console.log('File unlinked!');
        } catch (err) {
            console.log(err)
        }
    }
    const logo = imageUrl + 'assets/images/' + file.filename
    coin.logo = logo
    await coin.save();
    log.end();
    return coin
};

exports.create = create;
exports.getCoins = getCoins;
exports.getById = getById;
exports.coinDelete = coinDelete;
exports.update = update;
exports.uploadLogo = uploadLogo;


