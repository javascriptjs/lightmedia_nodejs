"use strict";
const mongoose = require("mongoose");

const coin = mongoose.Schema({
    coinName: { type: String, required: true },
    amount: { type: String, required: true },
    coinCode: { type: String, required: true },
    logo: { type: String },
    createdOn: { type: Date, default: Date.now },
    updatedOn: { type: Date, default: Date.now },
});

mongoose.model("coin", coin);
module.exports = coin;