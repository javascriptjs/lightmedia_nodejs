"use strict";
const mongoose = require("mongoose");

const project = mongoose.Schema({
    projectName: { type: String, required: true },
    createdOn: { type: Date, default: Date.now },
    updatedOn: { type: Date, default: Date.now },
});

mongoose.model("project", project);
module.exports = project;