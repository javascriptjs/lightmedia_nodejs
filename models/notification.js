"use strict";
const mongoose = require("mongoose");

const notification = mongoose.Schema({
    title: { type: String, required: true, trim: true, },
    body: { type: String, required: true, trim: true, },
    coin: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'coin'
    },
    plan: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'plan'
    },
    status: { type: String, default: "new", required: true },
    createdOn: { type: Date, default: Date.now },
    updatedOn: { type: Date, default: Date.now },
});

mongoose.model("notification", notification);
module.exports = notification;