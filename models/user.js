"use strict";
const mongoose = require("mongoose");

const user = mongoose.Schema({
    userName: { type: String, required: true, trim: true, },
    email: { type: String, required: true, trim: true, },
    password: { type: String, required: true, trim: true, },
    dob: { type: String, default: "" },
    phone: { type: String, default: "" },
    address: { type: String, default: "" },
    country: { type: String, default: "" },
    avatar: { type: String, default: "" }, 
    plan: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'plan'
    }, 
    coins: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'coin'
    }], 
    token: { type: String, default: "" }, 
    status: {
        type: String,
        default: "active",
        enum: ["active", "inactive"]
    },
    role: {
        type: String,
        default: "user",
        enum: ["user", "admin"]
    },
    deviceToken: { type: String, default: "" },
    createdOn: { type: Date, default: Date.now },
    updatedOn: { type: Date, default: Date.now },
});

mongoose.model("user", user);
module.exports = user;