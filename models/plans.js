"use strict";
const mongoose = require("mongoose");

const plan = mongoose.Schema({
    planName: { type: String, required: true, trim: true, },
    amount: { type: String, required: true, trim: true, },
    coinCount: { type: Number, required: true },
    createdOn: { type: Date, default: Date.now },
    updatedOn: { type: Date, default: Date.now },
});

mongoose.model("plan", plan);
module.exports = plan;